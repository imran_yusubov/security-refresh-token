package az.ingress.security.service;

import az.ingress.security.dto.PersonDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class DriverLicenseService {

    @PreAuthorize("#personDto.age >= 18")
    public String getDriverLicense(PersonDto personDto) {
        log.info("Service getting driver license {}", personDto);
        return "Driver License";
    }

    @PostAuthorize
            ("returnObject.age >=18")
    public PersonDto getDriverLicenseWithId(Long id) {
        log.info("Service getting driver license {}", id);
        return getPersonDto(id);
    }


    private PersonDto getPersonDto(Long id) {
        if (id == 1L) {
            PersonDto personDto = new PersonDto();
            personDto.setAge(20);
            personDto.setName("Nicat");
            return personDto;
        } else {
            PersonDto personDto = new PersonDto();
            personDto.setAge(15);
            personDto.setName("Farid");
            return personDto;

        }
    }
}
