package az.ingress.security.service;

import az.ingress.security.auth.domain.RefreshToken;
import az.ingress.security.auth.repository.RefreshTokenRepository;
import az.ingress.security.auth.repository.UserRepository;
import az.ingress.security.dto.RefreshTokenDto;
import az.ingress.security.dto.SignInRequestDto;
import az.ingress.security.dto.SignInResponseDto;
import az.ingress.security.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthService {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtService;
    private final RefreshTokenRepository refreshTokenRepository;
    private final UserRepository userRepository;

    public SignInResponseDto signIn(SignInRequestDto signInRequestDto) {
        log.info("Authenticating user {}", signInRequestDto.getUsername());
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(signInRequestDto.getUsername(),
                signInRequestDto.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        log.info("Authentication is {}", authentication);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new SignInResponseDto(jwtService.issueToken(authentication), issueRefreshToken(authentication, null));
    }


    private RefreshTokenDto issueRefreshToken(Authentication authentication, Long previousRefreshTokenId) {
        Calendar date = Calendar.getInstance();
        long timeInSecs = date.getTimeInMillis();
        Date afterAdding10Mins = new Date(timeInSecs + (10 * 60 * 1000));

        RefreshToken refreshToken = RefreshToken
                .builder()
                .username(authentication.getName())
                .token(UUID.randomUUID().toString())
                .eat(afterAdding10Mins)
                .valid(true)
                .previousRefreshTokenId(previousRefreshTokenId)
                .build();

        refreshTokenRepository.save(refreshToken);
        return new RefreshTokenDto(refreshToken.getToken(), refreshToken.getEat());
    }

    public SignInResponseDto authenticateWithRefreshToken(RefreshTokenDto refreshTokenDto) {
        log.info("Refresh token {}", refreshTokenDto.getToken());
        RefreshToken refreshToken = refreshTokenRepository.findByToken(refreshTokenDto.getToken())
                .orElseThrow(() -> new RuntimeException("Unauthorised"));


        if (refreshToken.isValid() && refreshToken.getEat().after(new Date())) {
            UserDetails byUsername = userRepository.findByUsername(refreshToken.getUsername());
            if (byUsername.isAccountNonExpired()
                    && byUsername.isAccountNonLocked()
                    && byUsername.isCredentialsNonExpired()
                    && byUsername.isEnabled()) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(byUsername.getUsername(), null, byUsername.getAuthorities());
                refreshToken.setValid(false);
                refreshTokenRepository.save(refreshToken);
                return new SignInResponseDto(jwtService.issueToken(usernamePasswordAuthenticationToken), issueRefreshToken(usernamePasswordAuthenticationToken, refreshToken.getId()));
            }
        }
        throw new RuntimeException("Unauthorised");
    }
}
