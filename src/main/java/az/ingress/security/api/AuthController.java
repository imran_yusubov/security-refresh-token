package az.ingress.security.api;

import az.ingress.security.dto.RefreshTokenDto;
import az.ingress.security.dto.SignInRequestDto;
import az.ingress.security.dto.SignInResponseDto;
import az.ingress.security.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/auth/sign-in")
    public SignInResponseDto signIn(@RequestBody @Valid SignInRequestDto signInRequestDto) {
        log.info("Authenticating user {}", signInRequestDto.getUsername());
        return authService.signIn(signInRequestDto);
    }

    @PostMapping("/auth/refresh-token")
    public SignInResponseDto signIn(@RequestBody @Valid RefreshTokenDto refreshTokenDto) {
        return authService.authenticateWithRefreshToken(refreshTokenDto);
    }
}
