package az.ingress.security.api;

import az.ingress.security.dto.PersonDto;
import az.ingress.security.service.DriverLicenseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/driver-license")
public class PersonController {

    private final DriverLicenseService driverLicenseService;

    @PostMapping
    //  @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getDriverLicense(@RequestBody @Valid PersonDto personDto) {
        log.info("Controller getting driver license {}", personDto);
        return driverLicenseService.getDriverLicense(personDto);
    }

    @GetMapping("/{id}")
    //  @PreAuthorize("hasRole('ROLE_ADMIN')")
    public PersonDto getDriverLicense(@PathVariable Long id) {
        log.info("Controller getting driver license {}", id);
        return driverLicenseService.getDriverLicenseWithId(id);
    }

}
