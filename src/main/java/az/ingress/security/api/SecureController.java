package az.ingress.security.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class SecureController {

    @GetMapping("/hello") // anyone
    public String sayHello() {
        return "Hello World";
    }

    @GetMapping("/hello2") // authenticated user
    public String sayHello2() {
        return "Hello World 2";
    }

    @GetMapping("/hello-user") // user with role USER
    public String sayHelloUser() {
        return "Hello User";
    }

    @GetMapping("/hello-admin") // user with role ADMIN
    public String sayHelloAdmin() {
        return "Hello ADMIN";
    }

}
