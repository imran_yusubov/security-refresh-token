package az.ingress.security.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
@RequiredArgsConstructor
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtAuthFilterConfigurerAdapter authFilterConfigurerAdapter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.apply(authFilterConfigurerAdapter);

        http.authorizeRequests()
                .antMatchers("/hello").permitAll()
                .antMatchers("/driver-license/**").permitAll()
                .antMatchers("/auth/sign-in").permitAll()
                .antMatchers("/auth/refresh-token").permitAll()
                .antMatchers("/hello-user").hasAnyRole("USER", "ADMIN")
                .antMatchers("/hello-admin").hasAnyRole("ADMIN")
                .and()
                .csrf().disable()
                .httpBasic();

        super.configure(http);
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("rufet")
//                .password(getPasswordEncoder().encode("1234"))
//                .roles("ADMIN")
//                .and()
//                .withUser("parviz")
//                .password(getPasswordEncoder().encode("1234"))
//                .roles("USER");
//    }


    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
