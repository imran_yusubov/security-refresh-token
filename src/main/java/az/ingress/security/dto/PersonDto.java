package az.ingress.security.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PersonDto {

    @NotBlank
    private String name;

    @NotNull
    private Integer age;
}
