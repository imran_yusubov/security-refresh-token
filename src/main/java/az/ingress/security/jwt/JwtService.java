package az.ingress.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class JwtService {

    private String secret = "your-256-bit-secretyour-256-bit-secretyour-256-bit-secretyour-256-bit-secretyour-256-bit-secretyour-256-bit-secret";
    private Key key;

    public JwtService() {
        byte[] keyBytes;
        keyBytes = secret.getBytes();
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public String issueToken(Authentication authentication) {
        Duration duration = Duration.ofSeconds(3600);
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(authentication.getName())
                .setIssuedAt(new Date())
                .claim("authorities", getAuthoritiesAsStr(authentication.getAuthorities()))
                .claim("pin", "2SJ4GNN")
                .claim("org", "MIA")
                .setExpiration(Date.from(Instant.now().plus(duration)))
                .signWith(key, SignatureAlgorithm.HS512);

        return jwtBuilder.compact();
    }

    public Claims parseToken(String token) {
        return Jwts.
                parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private List<String> getAuthoritiesAsStr(Collection<? extends GrantedAuthority> authorities) {
        return authorities
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
    }
}
