package az.ingress.security;

import az.ingress.security.auth.domain.Authority;
import az.ingress.security.auth.domain.User;
import az.ingress.security.auth.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class SecurityApplication implements CommandLineRunner {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user = new User();
        user.setUsername("rufet");
        user.setPassword(passwordEncoder.encode("1234"));
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setEnabled(true);
        user.setCredentialsNonExpired(true);

        Authority admin = new Authority();
        admin.setAuthority("ROLE_ADMIN");

        Authority userR = new Authority();
        userR.setAuthority("ROLE_ADMIN");

        user.setAuthorities(List.of(admin, userR));
        userRepository.save(user);
    }
}
